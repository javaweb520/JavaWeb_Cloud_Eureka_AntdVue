# JavaWeb_Cloud_Ant

#### Description
JavaWeb_Cloud_Ant微服务旗舰版框架是一款基于SpringCloud研发的分布式微服务框架，技术栈包括： SpringCloud、Vue、AntDesign、MybatisPlus，是一款精心打造的权限(RBAC)及内容管理系统，一款极简后台管理框架，可以一键CRUD生成所有模块代码，适配手机端、PAD端、PC电脑端，体验非常好，专注于为中小企业提供最佳的行业基础后台框架解决方案！

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
